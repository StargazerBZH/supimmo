pragma solidity ^0.7.4;

import "./Ownable.sol";
import "./SafeMath.sol";

contract EstateFactory is Ownable {

  using SafeMath for uint256;

  struct Estate {
    string name;
    string physicalAddress;
    uint squaredMeterSize;
    uint estateType;
    uint price;
    string[] images;
  }

  enum EstateType {
    HOUSE,
    FLAT,
    GARAGE,
    FIELD
  }

  mapping (address => uint) public ownerEstateCount;
  mapping (uint => address) public estateToOwner;

  Estate[] public estates;

  modifier onlyOwnerOf(uint _tokenId) {
    require(msg.sender == estateToOwner[_tokenId]);
    _;
  }

  function _createEstate(Estate memory estate) internal {
    estates.push(estate);
    uint id = estates.length.sub(1);
    estateToOwner[id] = msg.sender;
    ownerEstateCount[msg.sender] = ownerEstateCount[msg.sender].add(1);
  }
}